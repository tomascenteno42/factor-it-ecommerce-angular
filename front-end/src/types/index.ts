import { HttpHeaders, HttpParams } from '@angular/common/http';

//CART
export interface ICartItem {
  product: {
    id: number;
    name: string;
    price: number;
  };
  quantity: number;
}

export interface ICart {
  id: number;
  ownerId: number;
  type: CartType;
}

enum CartType {
  COMMON = 'COMMON',
  SPECIAL_DATED = 'SPECIAL_DATA',
}

//MODAL TOTAL
export interface ITotal {
  money: number;
  discounts: number;
  products: number;
}

//API
export interface IHttpOptions {
  headers?:
    | HttpHeaders
    | {
        [header: string]: string | string[];
      };
  observe?: 'body';
  params?:
    | HttpParams
    | {
        [param: string]: string | string[];
      };
  reportProgress?: boolean;
  responseType?: 'json';
  withCredentials?: boolean;
}

//USER
export interface IUser {
  id: number;
  vip: boolean;
  name: string;
}

export interface IUsers {
  users: IUser[];
  pages: number;
}

//PRODUCT
export interface IProduct {
  id: number;
  name: string;
  price: number;
}

//DATE
export interface IDate {
  month: number;
  day: number;
}
