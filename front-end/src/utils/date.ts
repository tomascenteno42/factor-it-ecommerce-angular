import { IDate } from 'src/types';

export const isSpecialDated = () => getCurrentDate().month === 11;

const getCurrentDate = (): IDate => {
  const date = new Date();
  return {
    month: date.getMonth() + 1,
    day: date.getDate(),
  };
};
