import { NgModule } from '@angular/core';

import { ApiModule } from '../api/api.module';
import { SharedModule } from '../shared/shared.module';

import { ProductsComponent } from './pages/products/products.component';
import { ProductComponent } from './components/product/product.component';

@NgModule({
  declarations: [ProductComponent, ProductsComponent],
  imports: [SharedModule, ApiModule],
})
export class ProductsModule {}
