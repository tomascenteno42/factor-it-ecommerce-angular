import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';

import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  constructor(private api: ApiService) {}

  public getProducts(skip?: string) {
    if (!skip) {
      skip = '0';
    }
    const params = new HttpParams().set('skip', skip);

    return this.api.get('products', { params });
  }
}
