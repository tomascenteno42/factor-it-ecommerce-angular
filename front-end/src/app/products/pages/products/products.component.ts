import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../../products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
})
export class ProductsComponent implements OnInit {
  public loading: boolean = false;

  public pages: number;
  public page: number;

  public products: any[];

  constructor(private productsService: ProductsService) {}

  ngOnInit(): void {
    this.bindProducts();
  }

  updateProductPage(p: number) {
    this.page = p - 1;
    this.bindProducts(this.page);
  }

  bindProducts(page?: number) {
    this.loading = true;

    if (!page || page === NaN) {
      page = 0;
    }

    this.productsService
      .getProducts((page * 10).toString())
      .subscribe((data) => {
        this.products = data['products'];
        this.pages = data['pages'];
        this.loading = false;
      });
  }
}
