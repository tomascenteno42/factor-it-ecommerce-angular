import { Component, Input } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from 'src/app/auth/auth.service';
import { CartService } from 'src/app/cart/cart.service';
import { CartItemsService } from 'src/app/cart-items/cart-items.service';

import { IProduct } from 'src/types';
import { isSpecialDated } from 'src/utils/date';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
})
export class ProductComponent {
  @Input() product: IProduct;
  public quantity: number = 1;

  constructor(
    private authService: AuthService,
    private cartService: CartService,
    private cartItemsService: CartItemsService,
    private toast: ToastrService
  ) {}

  addProductToCart() {
    let cartId = this.cartService.getCartId();

    if (!cartId) {
      const type = isSpecialDated() ? 'SPECIAL_DATED' : 'COMMON';
      const body = {
        type: type,
        ownerId: parseInt(this.authService.getUserId()),
      };

      this.toast.warning("You don't have a cart", 'FACTOR IT');

      this.cartService
        .createCart(body)
        .subscribe((data) => {
          if (data) {
            this.cartService.setCartId(data['id']);
            cartId = data['id'];
          }
        })
        .add(() => {
          this.toast.info('A cart was created for you', 'FACTOR IT');
          this.cartItemsService
            .addItemToCart(cartId, this.product.id, this.quantity)
            .subscribe()
            .add(() => {
              this.toast.success(
                `${this.product.name} was added to your cart`,
                'FACTOR IT'
              );
            });
        });
    } else {
      this.cartItemsService
        .addItemToCart(cartId, this.product['id'], this.quantity)
        .subscribe()
        .add(() => {
          this.toast.success(
            `${this.product.name} was added to your cart`,
            'FACTOR IT'
          );
        });
    }
  }
}
