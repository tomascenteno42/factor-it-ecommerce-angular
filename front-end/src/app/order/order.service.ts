import { Injectable } from '@angular/core';
import { IHttpOptions } from 'src/types';
import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private api: ApiService) {}

  placeOrder(cartId: string, body: any | null, options?: IHttpOptions) {
    return this.api.post(`cart/${cartId}/order`, body, options);
  }
}
