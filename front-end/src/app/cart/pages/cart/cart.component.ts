import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { UsersService } from 'src/app/users/users.service';
import { CartService } from '../../cart.service';
import { CartItemsService } from '../../../cart-items/cart-items.service';
import { OrderService } from '../../../order/order.service';

import { ICart, ICartItem, ITotal, IUser } from 'src/types';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
})
export class CartComponent implements OnInit {
  public loading = false;
  public show = false;

  public cartItems: ICartItem[];
  private cartId: string;
  private cart: ICart;
  private user: IUser;

  public total: ITotal = {
    money: 0,
    discounts: 0,
    products: 0,
  };

  constructor(
    private cartService: CartService,
    private cartItemsService: CartItemsService,
    private orderService: OrderService,
    private usersService: UsersService,
    private toast: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.cartId = this.cartService.getCartId();

    this.usersService.getUserBySessionId().subscribe((data: IUser) => {
      this.user = data;
    });

    this.cartService.getCartById().subscribe((data: ICart) => {
      this.cart = data;
    });

    this.loading = true;

    this.cartItemsService
      .getCartItems(this.cartId)
      .subscribe((data: ICartItem[]) => {
        this.cartItems = data;
        this.loading = false;
      });
  }

  calculateCheckoutTotal(): void {
    this.show = !this.show;

    this.total = {
      money: 0,
      discounts: 0,
      products: 0,
    };

    this.cartItems.forEach((item) => {
      if (item.quantity === 4) {
        this.total.discounts += item.product.price;
      }

      this.total.money += item.product.price * item.quantity;
      this.total.products += item.quantity;
    });
    if (!this.user.vip) {
      if (this.total.products >= 10) {
        if (this.cart.type === 'COMMON') {
          this.total.discounts += 100;
        } else {
          this.total.discounts += 500;
        }
      }
    } else {
      if (this.total.money > 5000) {
        this.total.discounts += 2000;
      }
    }
  }

  checkout() {
    const body = {
      total: this.total.money - this.total.discounts,
    };

    this.orderService
      .placeOrder(this.cartId, body)
      .subscribe()
      .add(() => {
        this.cartService.deleteCartId();
        this.router.navigate(['/products']);
        this.toast.success(
          `Your order of ${
            this.total.money - this.total.discounts
          }$ was placed correctly`,
          'FACTOR IT'
        );
      });
  }

  deleteCart() {
    this.cartService
      .deleteCart(this.cartId)
      .subscribe()
      .add(() => {
        this.cartService.deleteCartId();
        this.toast.success('Your cart was deleted', 'FACTOR IT');
        this.router.navigate(['/products']);
      });
  }

  deleteItem(event: ICartItem) {
    this.cartItems = this.cartItems.filter(
      (item) => item.product.id !== event.product.id
    );
  }
}
