import { NgModule } from '@angular/core';
import { CartItemsModule } from '../cart-items/cart-items.module';

import { SharedModule } from '../shared/shared.module';

import { CartComponent } from './pages/cart/cart.component';

@NgModule({
  declarations: [CartComponent],
  imports: [SharedModule, CartItemsModule],
})
export class CartModule {}
