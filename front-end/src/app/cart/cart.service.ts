import { Injectable } from '@angular/core';
import { IHttpOptions } from 'src/types';

import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private cartId: string;

  constructor(private api: ApiService) {}

  //METHODS THAT DO NOT INTERACT WITH API
  getCartId(): string {
    this.cartId = sessionStorage.getItem('CART ID');
    return sessionStorage.getItem('CART ID');
  }

  setCartId(id: string): void {
    sessionStorage.setItem('CART ID', id);
  }

  deleteCartId(): void {
    sessionStorage.removeItem('CART ID');
  }

  cartCreated(): boolean {
    return this.cartId === this.getCartId() && this.getCartId() !== null;
  }

  //METHODS THAT INTERACT WITH API
  getCartById() {
    return this.api.get(`cart/${this.cartId}`);
  }

  createCart(body: any | null, options?: IHttpOptions) {
    return this.api.post('cart', body, options);
  }

  deleteCart(cartId: string, options?: IHttpOptions) {
    return this.api.delete(`cart/${cartId}`, options);
  }
}
