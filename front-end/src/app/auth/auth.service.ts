import { Injectable } from '@angular/core';

//THIS SERVICE DO NOT INTERACT WITH THE BACK END API

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private userId: string;

  getUserId(): string {
    this.userId = sessionStorage.getItem('USER ID');
    return sessionStorage.getItem('USER ID');
  }

  setUserId(id: string): void {
    sessionStorage.setItem('USER ID', id);
  }

  deleteUserId(): void {
    sessionStorage.removeItem('USER ID');
  }

  isAuthenticated() {
    return this.userId === this.getUserId() && this.getUserId() !== null;
  }
}
