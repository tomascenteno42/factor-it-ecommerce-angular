import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from 'src/app/auth/auth.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
})
export class UserComponent {
  @Input() public user: any;

  constructor(
    private authService: AuthService,
    private toast: ToastrService,
    private router: Router
  ) {}

  logIn(id: string): void {
    this.authService.setUserId(id);
    this.router.navigate(['/products']);
    this.toast.success(
      `Logged in successfully. Hi ${this.user['name']}`,
      'FACTOR IT'
    );
  }
}
