import { AfterContentChecked, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from 'src/app/auth/auth.service';
import { UsersService } from '../../users.service';

import { IUser, IUsers } from 'src/types';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
})
export class UsersComponent implements OnInit, AfterContentChecked {
  public loading: boolean = false;

  public pages: number;
  public page: number;

  public usersData: IUser[];

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    private router: Router
  ) {}

  ngOnInit(): void {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/products']);
    } else {
      this.bindUsers();
    }
  }
  ngAfterContentChecked() {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/products']);
    }
  }

  updateUserPage(p: number) {
    this.page = p - 1;
    this.bindUsers(this.page);
  }

  bindUsers(page?: number) {
    this.loading = true;
    if (!page || page === NaN) {
      page = 0;
    }

    this.usersService
      .getUsers((page * 10).toString())
      .subscribe((data: IUsers) => {
        this.usersData = data.users;
        this.pages = data.pages;
        this.loading = false;
      });
  }
}
