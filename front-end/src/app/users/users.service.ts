import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IHttpOptions } from 'src/types';
import { ApiService } from '../api/api.service';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  constructor(private api: ApiService, private authService: AuthService) {}

  public getUsers(skip?: string) {
    if (!skip) {
      skip = '0';
    }
    const params = new HttpParams().set('skip', skip);
    return this.api.get('user/all', { params });
  }

  public getUserBySessionId(options?: IHttpOptions) {
    if (this.authService.isAuthenticated()) {
      const userId = this.authService.getUserId();

      return this.api.get(`user/${userId}`, options);
    }
  }
}
