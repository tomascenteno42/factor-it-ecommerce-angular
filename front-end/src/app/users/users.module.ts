import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';

import { UsersComponent } from './pages/users/users.component';
import { UserComponent } from './components/user/user.component';

@NgModule({
  declarations: [UserComponent, UsersComponent],
  imports: [SharedModule],
})
export class UsersModule {}
