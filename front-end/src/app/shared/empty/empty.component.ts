import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empty',
  templateUrl: './empty.component.html',
})
export class EmptyComponent {
  @Input('title') title: string;
  @Input('buttonContent') buttonContent: string;
  @Input('pageToRedirect') pageToRedirect: string;

  constructor(private router: Router) {}

  redirect() {
    this.router.navigate([`/${this.pageToRedirect}`]);
  }
}
