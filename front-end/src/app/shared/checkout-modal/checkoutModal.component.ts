import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

import { ICartItem, ITotal } from 'src/types';

@Component({
  selector: 'app-checkout-modal',
  templateUrl: './checkoutModal.component.html',
})
export class CheckoutModalComponent implements OnChanges {
  @Input() items: ICartItem[];
  @Input() show: boolean;
  @Input() total: ITotal;

  @Output('checkout') checkout: EventEmitter<any> = new EventEmitter();
  @Output() showChange: EventEmitter<boolean> = new EventEmitter();

  ngOnChanges(changes: SimpleChanges): void {
    for (const propName in changes) {
      if (propName === 'show') {
        this.show = changes.show.currentValue;
      }
    }
  }
}
