import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { CheckoutModalComponent } from './checkout-modal/checkoutModal.component';
import { EmptyComponent } from './empty/empty.component';
import { LoadingComponent } from './loading/loading.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PaginationComponent } from './pagination/pagination.component';

@NgModule({
  declarations: [
    CheckoutModalComponent,
    PaginationComponent,
    LoadingComponent,
    NavbarComponent,
    EmptyComponent,
  ],
  exports: [
    CheckoutModalComponent,
    PaginationComponent,
    LoadingComponent,
    NavbarComponent,
    EmptyComponent,
    CommonModule,
    RouterModule,
  ],
  imports: [CommonModule, RouterModule],
})
export class SharedModule {}
