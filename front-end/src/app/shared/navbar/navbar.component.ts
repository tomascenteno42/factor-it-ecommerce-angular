import {
  Component,
  OnInit,
  AfterViewChecked,
  AfterContentChecked,
  OnChanges,
  DoCheck,
} from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';

import { AuthService } from 'src/app/auth/auth.service';
import { CartService } from 'src/app/cart/cart.service';

import { isSpecialDated } from 'src/utils/date';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
})
export class NavbarComponent
  implements OnInit, AfterViewChecked, AfterContentChecked, OnChanges, DoCheck {
  userIsAuthenticated: boolean = false;
  cartCreated: boolean = false;

  constructor(
    private authService: AuthService,
    private cartService: CartService,
    private toast: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.bindUserAndCart();
  }

  ngOnChanges() {
    this.bindUserAndCart();
  }

  ngDoCheck() {
    this.bindUserAndCart();
  }

  ngAfterViewChecked() {
    this.bindUserAndCart();
  }

  ngAfterContentChecked() {
    this.bindUserAndCart();
  }

  logOut() {
    this.router.navigate(['/users']);
    this.authService.deleteUserId();
    this.cartService.deleteCartId();
    this.toast.success('Logged out successfully, see you soon', 'FACTOR IT');
  }

  createCart() {
    const ownerId = parseInt(this.authService.getUserId());
    const type = isSpecialDated() ? 'SPECIAL_DATED' : 'COMMON';
    const body = {
      type,
      ownerId,
    };
    this.cartService
      .createCart(body)
      .subscribe((cart) => {
        if (cart) {
          this.cartService.setCartId(cart['id']);
        }
      })
      .add(() => {
        this.toast.success(`${type} cart created. `, 'FACTOR IT');
      });
  }

  bindUserAndCart(): void {
    this.userIsAuthenticated = this.authService.isAuthenticated();
    this.cartCreated = this.cartService.cartCreated();
  }
}
