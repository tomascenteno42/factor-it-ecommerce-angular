import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
})
export class PaginationComponent implements OnInit {
  public page = 1;
  @Input() maxPages: number;

  @Output() pageChange: EventEmitter<number> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  nextPage() {
    this.page += 1;
    this.pageChange.emit(this.page);
  }
  previousPage() {
    this.page -= 1;
    this.pageChange.emit(this.page);
  }
}
