import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';
import { CartGuard } from './cart/cart.guard';

import { UsersComponent } from './users/pages/users/users.component';
import { CartComponent } from './cart/pages/cart/cart.component';
import { ProductsComponent } from './products/pages/products/products.component';

const routes: Routes = [
  { path: 'products', canActivate: [AuthGuard], component: ProductsComponent },
  {
    path: 'cart',
    canActivate: [AuthGuard, CartGuard],
    component: CartComponent,
  },
  { path: 'users', component: UsersComponent },
  { path: '', pathMatch: 'full', redirectTo: 'products' },
  { path: '**', pathMatch: 'full', redirectTo: 'products' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
