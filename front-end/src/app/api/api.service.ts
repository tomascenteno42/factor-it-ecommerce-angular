import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { IHttpOptions } from 'src/types';

import { environment } from '../../environments/environment';

@Injectable()
export class ApiService {
  private url: string;

  constructor(private http: HttpClient) {
    this.setUrl();
  }

  public get(query: string, options?: IHttpOptions) {
    const url = this.url + query;
    return this.http.get(url, options);
  }

  public post(query: string, body: any | null, options?: IHttpOptions) {
    const url = this.url + query;
    return this.http.post(url, body, options);
  }

  public patch(query: string, body: any | null, options?: IHttpOptions) {
    const url = this.url + query;
    return this.http.patch(url, body, options);
  }

  public delete(query: string, options?: IHttpOptions) {
    const url = this.url + query;
    return this.http.delete(url, options);
  }

  setUrl(): void {
    this.url = environment.apiUrl;
  }
}
