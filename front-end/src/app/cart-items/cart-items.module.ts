import { NgModule } from '@angular/core';

import { CartItemComponent } from './components/cart-item/cart-item.component';

@NgModule({
  declarations: [CartItemComponent],
  exports: [CartItemComponent],
})
export class CartItemsModule {}
