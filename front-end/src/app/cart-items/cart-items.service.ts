import { Injectable } from '@angular/core';
import { IHttpOptions } from 'src/types';

import { ApiService } from '../api/api.service';

@Injectable({
  providedIn: 'root',
})
export class CartItemsService {
  constructor(private api: ApiService) {}

  getCartItems(cartId: string, options?: IHttpOptions) {
    if (cartId) {
      return this.api.get(`cart/${cartId}/products`, options);
    }
  }

  addItemToCart(
    cartId: string,
    itemId: number,
    quantity: number,
    options?: IHttpOptions
  ) {
    const body = {
      quantity,
    };
    return this.api.post(`cart/${cartId}/products/${itemId}`, body, options);
  }

  updateItemQuantity(
    cartId: string,
    itemId: number,
    quantity: number,
    options?: IHttpOptions
  ) {
    const body = {
      quantity,
    };
    return this.api.patch(`cart/${cartId}/products/${itemId}`, body, options);
  }

  deleteItemFromCart(cartId: string, itemId: number, options?: IHttpOptions) {
    return this.api.delete(`cart/${cartId}/products/${itemId}`, options);
  }
}
