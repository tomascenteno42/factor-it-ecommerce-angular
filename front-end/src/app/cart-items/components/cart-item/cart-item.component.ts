import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ToastrService } from 'ngx-toastr';

import { CartItemsService } from '../../cart-items.service';

import { ICartItem } from 'src/types';
import { CartService } from 'src/app/cart/cart.service';

@Component({
  selector: 'app-cart-item',
  templateUrl: './cart-item.component.html',
})
export class CartItemComponent implements OnInit {
  @Input() item: ICartItem;
  @Output() itemDeleted: EventEmitter<ICartItem> = new EventEmitter();

  private cartId: string;

  constructor(
    private cartService: CartService,
    private cartItemsService: CartItemsService,
    private toast: ToastrService
  ) {}

  ngOnInit(): void {
    this.cartId = this.cartService.getCartId();
  }

  increaseItemQuantity() {
    this.item.quantity += 1;
    this.cartItemsService
      .updateItemQuantity(
        this.cartId,
        this.item.product['id'],
        this.item.quantity
      )
      .subscribe();
  }

  decreaseItemQuantity() {
    if (this.item.quantity - 1 >= 1) {
      this.item.quantity -= 1;
      this.cartItemsService
        .updateItemQuantity(
          this.cartId,
          this.item.product['id'],
          this.item.quantity
        )
        .subscribe();
    }
  }

  deleteItemFromCart() {
    this.cartItemsService
      .deleteItemFromCart(this.cartId, this.item.product.id)
      .subscribe((res) => {
        if (res) {
          this.itemDeleted.emit(this.item);
        }
      })
      .add(() => {
        this.toast.success(
          `${this.item.product.name} was deleted from your cart`,
          'FACTOR IT'
        );
      });
  }
}
